module.exports = function(grunt) {
  'use strict';

  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    sassdoc: {
      default: {
        src: 'stylesheets',
        dest: 'docs/sass',
        options: {
          package: './package.json'
        }
      }
    }
  });
};
