# Paths
# With Compass, "project_path" is acutally the public http path
root_path       = File.realpath(File.join(File.dirname(__FILE__), "../"))
project_path    = File.join(root_path, "public")
css_dir         = "assets/css"
javascripts_dir = "assets/js"
sass_dir        = "assets/sass"
images_dir      = "assets/img"
fonts_dir       = "assets/fonts"
extensions_path = File.join(root_path, "node_modules")

# Import paths
add_import_path extensions_path
add_import_path Sass::CssImporter::Importer.new(extensions_path)

# So we can import Easel itself
add_import_path File.realpath(File.join(File.dirname(__FILE__), "../../stylesheets"))

# Settings
output_style     = :expanded
line_comments    = false
disable_warnings = false
sourcemap        = true
